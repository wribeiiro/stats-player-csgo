<?php
if (empty($_SESSION['steam_uptodate']) or empty($_SESSION['steam_personaname'])):
	require 'SteamConfig.php';

	$url = file_get_contents("https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=".$steamauth['apikey']."&steamids=".$_SESSION['steamid']); 

	$url2 = file_get_contents("http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=".$steamauth['apikey']."&steamid=".$_SESSION['steamid']);

	$content  = json_decode($url, true); // profile steam
	$content2 = json_decode($url2, true); // profile cs go

	$_SESSION['csgo_total_kills']       = $content2['playerstats']['stats'][0]['value'];
	$_SESSION['csgo_total_mortes']      = $content2['playerstats']['stats'][1]['value'];
	$_SESSION['csgo_total_plants']		= $content2['playerstats']['stats'][3]['value'];
	$_SESSION['csgo_total_defuses']     = $content2['playerstats']['stats'][4]['value'];
	$_SESSION['csgo_total_wins']        = $content2['playerstats']['stats'][5]['value'];
	$_SESSION['csgo_total_hs']          = $content2['playerstats']['stats'][24]['value'];
	$_SESSION['csgo_total_pistol']      = $content2['playerstats']['stats'][26]['value'];
	$_SESSION['csgo_total_mvp']         = $content2['playerstats']['stats'][91]['value'];


/*************************************************************************************/

	$_SESSION['steam_steamid'] 			= $content['response']['players'][0]['steamid'];
	$_SESSION['steam_communityvisibilitystate'] = $content['response']['players'][0]['communityvisibilitystate'];
	$_SESSION['steam_profilestate']     = $content['response']['players'][0]['profilestate'];
	$_SESSION['steam_personaname']      = $content['response']['players'][0]['personaname'];
	$_SESSION['steam_lastlogoff'] 	    = $content['response']['players'][0]['lastlogoff'];
	$_SESSION['steam_profileurl'] 		= $content['response']['players'][0]['profileurl'];
	$_SESSION['steam_avatar'] 		    = $content['response']['players'][0]['avatar'];
	$_SESSION['steam_avatarmedium']     = $content['response']['players'][0]['avatarmedium'];
	$_SESSION['steam_avatarfull']       = $content['response']['players'][0]['avatarfull'];
	$_SESSION['steam_personastate']     = $content['response']['players'][0]['personastate'];
	if (isset($content['response']['players'][0]['realname'])):
		$_SESSION['steam_realname'] = $content['response']['players'][0]['realname'];
	else:
		$_SESSION['steam_realname'] = "Real name not given";
	endif;

	$_SESSION['steam_primaryclanid'] = $content['response']['players'][0]['primaryclanid'];
	$_SESSION['steam_timecreated']   = $content['response']['players'][0]['timecreated'];
	$_SESSION['steam_uptodate']      = time();
endif;

$steamprofile['steamid'] 				  = $_SESSION['steam_steamid'];
$steamprofile['communityvisibilitystate'] = $_SESSION['steam_communityvisibilitystate'];
$steamprofile['profilestate'] 			  = $_SESSION['steam_profilestate'];
$steamprofile['personaname']              = $_SESSION['steam_personaname'];
$steamprofile['lastlogoff']               = $_SESSION['steam_lastlogoff'];
$steamprofile['profileurl']               = $_SESSION['steam_profileurl'];
$steamprofile['avatar']                   = $_SESSION['steam_avatar'];
$steamprofile['avatarmedium']             = $_SESSION['steam_avatarmedium'];
$steamprofile['avatarfull']               = $_SESSION['steam_avatarfull'];
$steamprofile['personastate']             = $_SESSION['steam_personastate'];
$steamprofile['realname']                 = $_SESSION['steam_realname'];
$steamprofile['primaryclanid']            = $_SESSION['steam_primaryclanid'];
$steamprofile['timecreated'] 			  = $_SESSION['steam_timecreated'];
$steamprofile['uptodate'] 				  = $_SESSION['steam_uptodate'];

$csgoprofile['total_kills']               = $_SESSION['csgo_total_kills'];
$csgoprofile['total_mortes']              = $_SESSION['csgo_total_mortes'];
$csgoprofile['total_defuses']		      = $_SESSION['csgo_total_defuses'];
$csgoprofile['total_wins']				  = $_SESSION['csgo_total_wins'];

$csgoprofile['total_plants']		      = $_SESSION['csgo_total_plants'];
$csgoprofile['total_mvp']		     	  = $_SESSION['csgo_total_mvp'];
$csgoprofile['total_hs']		     	  = $_SESSION['csgo_total_hs'];
$csgoprofile['total_pistol']		      = $_SESSION['csgo_total_pistol'];

// Version 4.0
?>