<?php require ('steamauth/steamauth.php');?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Autenticação - Stats</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <style>
        .navbar {
            background: #303030;
        }

        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            height: 60px;
            background-color: #f5f5f5;
        }

        .container .text-muted {
            margin: 20px 0;
            color: #fff;
        }

        .container .text-muted a {
            color: #fff;
            text-decoration: none;
        }

        .footer > .container {
            padding-right: 15px;
            padding-left: 15px;
            background: #303030;
        }

        code {
            font-size: 80%;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="color: #fff">CS GO Stats</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#" style="color: #fff">Sobre</a></li>
                    <li><a href="#" style="color: #fff">Contato</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <?php
                            loginbutton();
                        ?>  
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
    <div class="container">
        <div class="jumbotron">
            <p><a class="btn btn-lg btn-primary" href="#" role="button">
                <img class="img-responsive" src="img/bg.png">
            </a></p>
        </div>
    </div>
    <footer class="footer">
        <div class="container" style="width: 100% !important">
            <div class="col-md-12">
                <p class="text-muted">Dev by: <a href="http:\\www.wribeiiro.com.br" target="_blank">Wellisson Ribeiro</a></p>
            </div>  
        </div>
    </footer>   
</body>
</html>