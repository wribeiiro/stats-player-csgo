<?php 
	require 'steamauth/steamauth.php';
	require 'steamauth/userInfo.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Stats CSGO - <?=$steamprofile['personaname']?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <style>
        .navbar {
            background: #303030;
        }

        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            height: 60px;
            background-color: #f5f5f5;
        }

        .container .text-muted {
            margin: 20px 0;
            color: #fff;
        }

        .container .text-muted a {
            color: #fff;
            text-decoration: none;
        }

        .footer > .container {
            padding-right: 15px;
            padding-left: 15px;
            background: #303030;
        }

        code {
            font-size: 80%;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top" style="width: 100%">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="color: #fff">CS GO Stats</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#" style="color: #fff">Sobre</a></li>
                    <li><a href="#" style="color: #fff">Contato</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img width="30" style="margin: 7px" src='<?=$steamprofile['avatarfull']?>'> <span class="caret" style="color: #fff"></span></a>
                        <ul class="dropdown-menu">
                            <li style="text-align: center;"><?php logoutbutton(); ?></li>
                        </ul>
                    </li>    
                </ul>
            </div><!--/.nav-collapse -->
        </nav>
    </div>
    <div class="container" >
        <div class="row">
            <h3><b>Nick:</b> <?=$steamprofile['personaname'];?></h3>
            <h3><b>Nome: </b> <?=$steamprofile['realname'];?></h3>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-3 text-center">
                <img src="img/kill.jpg" width="100">
                <h3>Total Kills:</h3>
                <h3><?=$csgoprofile['total_kills']?></h3>
            </div>
            <div class="col-md-3 text-center">
                <img src="img/death.png" width="100">
                <h3>Total Mortes</h3>
                <h3><?=$csgoprofile['total_mortes']?></h3>
            </div>
            <div class="col-md-3 text-center">
                <img src="img/win.png" width="100">
                <h3>Total Vitorias</h3>
                <h3><?=$csgoprofile['total_wins']?></h3>
            </div>
            <div class="col-md-3 text-center">
                <img src="img/defuse.png" width="100">
                <h3>Total Defuses</h3>
                <h3><?=$csgoprofile['total_defuses']?></h3>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3 text-center">
                <img src="img/bomb.png" class="text-center" width="113">
                <h3>Total Bombas Plantadas</h3>
                <h3><?=$csgoprofile['total_plants']?></h3>
            </div>
            <div class="col-md-3 text-center">
                <img src="img/mvp.png" width="100">
                <h3>Total MVP's</h3>
                <h3><?=$csgoprofile['total_mvp']?></h3>
            </div>
            <div class="col-md-3 text-center">
                <img src="img/hs.jpg" width="100">
                <h3>Total Headshots</h3>
                <h3><?=$csgoprofile['total_hs']?></h3>
            </div>
            <div class="col-md-3 text-center">
                <img src="img/pistol.png" width="100">
                <h3>Total Vitórias Pistol</h3>
                <h3><?=$csgoprofile['total_pistol']?></h3>
            </div>
        </div>
    </div>
    <br><br><br>
</body>
</html>